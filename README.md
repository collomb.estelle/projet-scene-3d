**3D Project**  
Isée Besombes, Estelle Collomb, Yann Vincent  


**Libraries:**  
Basic libraries used are: OpenGL, glfw, Assimpcy.  


**Implementation and difficulties:**  
First of all we had some issues loading fbx objects with their textures.  
Then the first thing was to create a ground that represents a pasture, as we didn´t want to spend too much time doing it by hand with triangles we created a mesh plane and deformed it to add relief with blender.  
To load it we create a _class Ground_ in a file _viewer.py_ so it is clearer.  

Then we had to load our elements, we also created a class for each object and placed them on the scene either with keyframe functions or with the Node function. We add:  
1) The entrance of a castle with two guards:  
    - the castle is loaded with texture,
    - the guards are loaded with a skinning animation so they hold their shield and sword and also with texture with load function _load_skinned_ modify in sush a way we have both at the same time,
2) Two archers who shoot arrows on a target:
    - the arrows move thanks to keyframe animations,  
    - the target is colored and illuminated with the Phong model,  
    - the archers are loaded with their texture,
3) A man with a catapult who shoot cannonballs on wolves attacking the castle:  
    - the cannon ball moves thanks to keyboard with a new function called _TranslationControlNode_,  
    - the catapult is created with a hierarchical model made from rectangles designed on blender plus we can control the rotation of the upper part with up and down arrows of the keyboard,  
    - the man is loaded with its texture,
4) A car and a man with a wooden club:  
    - the club has been created on blender using a mesh to model it and can be controlled with keyboard,  
    - the man is textured,
    - the car has been made with 2 boxes and 4 cylinders all first made on blender to have good proportions and then loaded with png textures.  
    
Finally after many tries, to make our Skybox we created a cube on blender and still on blender we flipped the normals so when we apply the landscape texture it is inside the cube and not outside as usual.  


**How to execute the code:**  
Command to launch the scene: _python3 viewer.py_.  
Command to:  
            1) move the catapult: _UP_ and _DOWN_ arrows on the keyboard to make it rotate from bottom to top.  
            2) move the cannonball: _LEFT_ and _RIGHT_ arrows on the keyboard to make the ball go forward and higher.  
            3) relaunch the arrows: _R_ on the keyboard.  
            4) hit the car with the club: _C_ and _B_ on the keyboard.  


**Who did what:**  
Yann made the function RotationControleNode works for the catapult (after translation and scale).  
Isée created the necessary objects on blender.  
Estelle & Isée worked on the same computer to code the rest of the project.  


**What we could have done better**  
- We could have generate a mesh to create the ground creating a function,
- Make a skinning animation adding animations in the existing functions of skinning,  
- Improve the skinned/texture loader adjusting the textures' positions,  
- Add others effects such as water or dust working with buffers,
