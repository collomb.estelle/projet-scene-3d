#version 330 core

// receiving interpolated color for fragment shader
in vec3 fragment_color;

// output fragment color for OpenGL
out vec4 out_color;

uniform sampler2D diffuse_map;
in vec2 frag_tex_coords;

void main() {
    out_color = texture(diffuse_map, frag_tex_coords);
}
