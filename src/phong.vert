#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

uniform mat4 model, view, projection;

out vec3 w_position, w_normal;  

void main() {
    gl_Position = projection * view * model * vec4(position, 1.0);
    w_position = (model*vec4(position,1)).xyz;
    w_normal = (model * vec4(normal, 0)).xyz;
}
