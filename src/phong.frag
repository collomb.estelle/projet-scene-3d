#version 330 core

in vec3 w_position, w_normal;  

uniform vec3 light_dir;

uniform vec3 k_d;
uniform vec3 k_a;
uniform vec3 k_s;
uniform float s;

uniform vec3 w_camera_position;

out vec4 out_color;

void main() {
    vec3 n = normalize(w_normal);

    vec3 r = normalize(reflect(light_dir,n));
    
    vec3 v = normalize(w_camera_position - w_position);
    out_color = vec4(k_a+vec3(0.8,0,0) * max(0, dot(n, -light_dir))+k_s*pow(max(0, dot(r,v)),s),1);
}
