#version 330 core

// ---- camera geometry
uniform mat4 projection, view, model;

// ---- skinning globals and attributes
const int MAX_VERTEX_BONES=4, MAX_BONES=128;
uniform mat4 bone_matrix[MAX_BONES];

// ---- vertex attributes
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 color;
layout(location = 2) in vec4 bone_ids;
layout(location = 3) in vec4 bone_weights;
layout(location = 4) in vec2 uvs;
layout(location = 6) in vec3 position1;

// ----- interpolated attribute variables to be passed to fragment shader
out vec3 fragment_color;

out vec2 frag_tex_coords;

void main() {

    mat4 skin_matrix = mat4(0);
    if (bone_weights == vec4(0))
        skin_matrix = model;
    else {
    for (int b=0; b < MAX_VERTEX_BONES; b++)
        skin_matrix +=  bone_weights[b] * bone_matrix[int(bone_ids[b])];
    }

    vec4 w_position4 = skin_matrix * vec4(position, 1.0);

    gl_Position = projection * view * model * vec4(position,1);
    frag_tex_coords = uvs;
}
