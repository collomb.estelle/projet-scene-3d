from PIL import Image               
from itertools import cycle
...

#!/usr/bin/env python
# Python built-in modules
import os                           
from itertools import cycle
import sys

# External, non built-in modules
import OpenGL.GL as GL              
import glfw                        
import numpy as np                  
import assimpcy                    

from core import Shader, Mesh, Node, Viewer
from transform import rotate, scale, translate, identity
from transform import (quaternion_slerp, quaternion_matrix, quaternion,
                       quaternion_from_euler)

from transform import lerp, vec
from bisect import bisect_left

MAX_VERTEX_BONES = 4
MAX_BONES = 128

class PhongMesh(Mesh):
    def __init__(self, shader, attributes, index=None,
                 light_dir=(0, -1, 0),   
                 k_a=(0, 0, 0), k_d=(1, 1, 0), k_s=(1, 1, 1), s=16.):
        super().__init__(shader, attributes, index)
        self.light_dir = light_dir
        self.k_a, self.k_d, self.k_s, self.s = k_a, k_d, k_s, s
        names = ['light_dir', 'k_a', 's', 'k_s', 'k_d', 'w_camera_position']
        loc = {n: GL.glGetUniformLocation(shader.glid, n) for n in names}
        self.loc.update(loc)

    def draw(self, projection, view, model, primitives=GL.GL_TRIANGLES):
        GL.glUseProgram(self.shader.glid)
        GL.glUniform3fv(self.loc['light_dir'], 1, self.light_dir)
        GL.glUniform3fv(self.loc['k_a'], 1, self.k_a)
        GL.glUniform3fv(self.loc['k_d'], 1, self.k_d)
        GL.glUniform3fv(self.loc['k_s'], 1, self.k_s)
        GL.glUniform1f(self.loc['s'], max(self.s, 0.001))
        w_camera_position = np.linalg.inv(view)[:,3]
        GL.glUniform3fv(self.loc['w_camera_position'], 1, w_camera_position)
        super().draw(projection, view, model, primitives)

class Texture:
    def __init__(self, tex_file, wrap_mode=GL.GL_REPEAT, min_filter=GL.GL_LINEAR,
                 mag_filter=GL.GL_LINEAR_MIPMAP_LINEAR):
        self.glid = GL.glGenTextures(1)
        try:
            tex = np.asarray(Image.open(tex_file).convert('RGBA'))
            GL.glBindTexture(GL.GL_TEXTURE_2D, self.glid)
            GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, tex.shape[1],
                            tex.shape[0], 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, tex)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, wrap_mode)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, wrap_mode)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, min_filter)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, mag_filter)
            GL.glGenerateMipmap(GL.GL_TEXTURE_2D)
            message = 'Loaded texture %s\t(%s, %s, %s, %s)'
            print(message % (tex_file, tex.shape, wrap_mode, min_filter, mag_filter))
        except FileNotFoundError:
            print("ERROR: unable to load texture file %s" % tex_file)

    def __del__(self): 
        GL.glDeleteTextures(self.glid)


class TexturedPlane(Mesh):
    def __init__(self, tex_file, shader):

        vertices = 100 * np.array(
            ((-1, -1, 0), (1, -1, 0), (1, 1, 0), (-1, 1, 0)), np.float32)
        faces = np.array(((0, 1, 2), (0, 2, 3)), np.uint32)
        super().__init__(shader, [vertices], faces)
        loc = GL.glGetUniformLocation(shader.glid, 'diffuse_map')
        self.loc['diffuse_map'] = loc
        self.wrap = cycle([GL.GL_REPEAT, GL.GL_MIRRORED_REPEAT,
                           GL.GL_CLAMP_TO_BORDER, GL.GL_CLAMP_TO_EDGE])
        self.filter = cycle([(GL.GL_NEAREST, GL.GL_NEAREST),
                             (GL.GL_LINEAR, GL.GL_LINEAR),
                             (GL.GL_LINEAR, GL.GL_LINEAR_MIPMAP_LINEAR)])
        self.wrap_mode, self.filter_mode = next(self.wrap), next(self.filter)
        self.tex_file = tex_file
        self.texture = Texture(tex_file, self.wrap_mode, *self.filter_mode)

    def key_handler(self, key):
        if key == glfw.KEY_F6:
            self.wrap_mode = next(self.wrap)
            self.texture = Texture(self.tex_file, self.wrap_mode, *self.filter_mode)
        if key == glfw.KEY_F7:
            self.filter_mode = next(self.filter)
            self.texture = Texture(self.tex_file, self.wrap_mode, *self.filter_mode)

    def draw(self, projection, view, model, primitives=GL.GL_TRIANGLES):
        GL.glUseProgram(self.shader.glid)
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture.glid)
        GL.glUniform1i(self.loc['diffuse_map'], 0)
        super().draw(projection, view, model, primitives)

class TexturedMesh(Mesh):
    def __init__(self, shader, texture, attributes, index=None):
        super().__init__(shader, attributes, index)
        loc = GL.glGetUniformLocation(shader.glid, 'diffuse_map')
        self.loc['diffuse_map'] = loc
        self.texture = texture
        
    def draw(self, projection, view, model, primitives=GL.GL_TRIANGLES):
        GL.glUseProgram(self.shader.glid)
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture.glid)
        GL.glUniform1i(self.loc['diffuse_map'], 0)
        super().draw(projection, view, model, primitives)
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)
        GL.glUseProgram(0)
         
class KeyFrames:
    def __init__(self, time_value_pairs, interpolation_function=lerp):
        if isinstance(time_value_pairs, dict): 
            time_value_pairs = time_value_pairs.items()
        keyframes = sorted(((key[0], key[1]) for key in time_value_pairs))
        self.times, self.values = zip(*keyframes)  
        self.interpolate = interpolation_function
    def value(self, time):
        if time <= self.times[0]:
            return self.values[0]
        if time >= self.times[-1]:
            return self.values[-1]
        t_i = bisect_left(self.times, time) - 1     
        f = (time - self.times[t_i]) / (self.times[t_i + 1] - self.times[t_i])
        return self.interpolate(self.values[t_i], self.values[t_i + 1], f)

class TransformKeyFrames:
    def __init__(self, translate_keys, rotate_keys, scale_keys):
        self.t = KeyFrames(translate_keys)
        self.r = KeyFrames(rotate_keys, quaternion_slerp)
        self.s = KeyFrames(scale_keys)
    def value(self, time):
        T = self.t.value(time)
        S = self.s.value(time)
        R = quaternion_matrix(self.r.value(time))
        return translate(T) @ R @ scale(S)

#-----Key frame animation function-----
class KeyFrameControlNode(Node):
    def __init__(self, translate_keys, rotate_keys, scale_keys):
        super().__init__()
        self.keyframes = TransformKeyFrames(translate_keys, rotate_keys, scale_keys)
    def draw(self, projection, view, model):
        self.transform = self.keyframes.value(glfw.get_time())
        super().draw(projection, view, model)

#-----Key board animation rotation function-----
class RotationControlNode(Node):
    def __init__(self, key_up, key_down, axis, angle=0):
        super().__init__(transform=rotate(axis, angle))
        self.angle, self.axis = angle, axis
        self.key_up, self.key_down = key_up, key_down
    def key_handler(self, key):
        self.angle += 5 * int(key == self.key_up)
        self.angle -= 5 * int(key == self.key_down)
        self.transform = rotate(self.axis, self.angle)
        super().key_handler(key)

#-----Key board translation animation function-----
#This class allows us to control the translation of an object with a key
class TranslationControlNode(Node):
    def __init__(self, key_up, key_down, x, y, z):
        super().__init__()
        self.x = x
        self.y = y
        self.z = z
        self.key_up, self.key_down = key_up, key_down
    def key_handler(self, key):
        self.x -= 8 * int(key == self.key_down) #the object moves from 8 units on x axis in negative sens
        self.x -= 8 * int(key == self.key_up)   #the object moves from 8 units on x axis in negative sens
        self.y -= 5 * int(key == self.key_down) #the object moves from 5 units on y axis in negative sens
        self.y += 5 * int(key == self.key_up)   #the object moves from 5 units on y axis in positive sens
        self.transform = translate(self.x,self.y,self.z)
        super().key_handler(key)

#We merged SkinnedMesh by mixing skinned and textured meshes
class SkinnedMesh(Mesh):
    def __init__(self, shader, attribs, bone_nodes, bone_offsets, texture,index=None):
        super().__init__(shader, attribs, index)
        loc = GL.glGetUniformLocation(shader.glid, 'diffuse_map')
        self.loc['diffuse_map'] = loc
        self.texture = texture
        self.bone_nodes = bone_nodes
        self.bone_offsets = np.array(bone_offsets, np.float32)
    def draw(self, projection, view, model, primitives=GL.GL_TRIANGLES):
        GL.glUseProgram(self.shader.glid)
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture.glid)
        GL.glUniform1i(self.loc['diffuse_map'], 0)
        world_transforms = [node.world_transform for node in self.bone_nodes]
        bone_matrix = world_transforms @ self.bone_offsets
        loc = GL.glGetUniformLocation(self.shader.glid, 'bone_matrix')
        GL.glUniformMatrix4fv(loc, len(self.bone_nodes), True, bone_matrix)
        super().draw(projection, view, model, primitives)
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)
        GL.glUseProgram(0)

class SkinningControlNode(Node):
    def __init__(self, *keys, transform=identity()):
        super().__init__(transform=transform)
        self.keyframes = TransformKeyFrames(*keys) if keys[0] else None
        self.world_transform = identity()
    def draw(self, projection, view, model):
        if self.keyframes: 
            self.transform = self.keyframes.value(glfw.get_time())
        self.world_transform = model @ self.transform
        super().draw(projection, view, model)

# -------------- 3D resource loader -------------------------------------------
def load_phong_mesh(file, shader, light_dir):
    """ load resources from file using assimp, return list of ColorMesh """
    try:
        pp = assimpcy.aiPostProcessSteps
        flags = pp.aiProcess_Triangulate | pp.aiProcess_GenSmoothNormals
        scene = assimpcy.aiImportFile(file, flags)
    except assimpcy.all.AssimpError as exception:
        print('ERROR loading', file + ': ', exception.args[0].decode())
        return []
    meshes = []
    for mesh in scene.mMeshes:
        mat = scene.mMaterials[mesh.mMaterialIndex].properties
        mesh = PhongMesh(shader, [mesh.mVertices, mesh.mNormals], mesh.mFaces,
                         k_d=mat.get('COLOR_DIFFUSE', (1, 1, 1)),
                         k_s=mat.get('COLOR_SPECULAR', (1, 1, 1)),
                         k_a=mat.get('COLOR_AMBIENT', (0, 0, 0)),
                         s=mat.get('SHININESS', 16.),
                         light_dir=light_dir)
        meshes.append(mesh)
    size = sum((mesh.mNumFaces for mesh in scene.mMeshes))
    print('Loaded %s\t(%d meshes, %d faces)' % (file, len(meshes), size))
    return meshes

def load_textured(file, shader, tex_file=None):
    try:
        pp = assimpcy.aiPostProcessSteps
        flags = pp.aiProcess_Triangulate | pp.aiProcess_FlipUVs
        scene = assimpcy.aiImportFile(file, flags)
    except assimpcy.all.AssimpError as exception:
        print('ERROR loading', file + ': ', exception.args[0].decode())
        return []
    path = os.path.dirname(file) if os.path.dirname(file) != '' else './'
    for mat in scene.mMaterials:
        if not tex_file and 'TEXTURE_BASE' in mat.properties:  
            name = os.path.basename(mat.properties['TEXTURE_BASE'])
            paths = os.walk(path, followlinks=True)
            found = [os.path.join(d, f) for d, _, n in paths for f in n
                    if name.startswith(f) or f.startswith(name)]
            assert found, 'Cannot find texture %s in %s subtree' % (name, path)
            tex_file = found[0]
        if tex_file:
            mat.properties['diffuse_map'] = Texture(tex_file=tex_file)
    meshes = []
    for mesh in scene.mMeshes:
        mat = scene.mMaterials[mesh.mMaterialIndex].properties
        assert mat['diffuse_map'], "Trying to map using a textureless material"
        attributes = [mesh.mVertices, mesh.mTextureCoords[0],mesh.mNormals,mesh.mFaces]
        mesh = TexturedMesh(shader, mat['diffuse_map'], attributes, mesh.mFaces)
        meshes.append(mesh)
    size = sum((mesh.mNumFaces for mesh in scene.mMeshes))
    print('Loaded %s\t(%d meshes, %d faces)' % (file, len(meshes), size))
    return meshes

def load(file, shader):
    try:
        pp = assimpcy.aiPostProcessSteps
        flags = pp.aiProcess_Triangulate | pp.aiProcess_GenSmoothNormals
        scene = assimpcy.aiImportFile(file, flags)
    except assimpcy.all.AssimpError as exception:
        print('ERROR loading', file + ': ', exception.args[0].decode())
        return []
    meshes = [Mesh(shader, [m.mVertices, m.mNormals], m.mFaces)
              for m in scene.mMeshes]
    size = sum((mesh.mNumFaces for mesh in scene.mMeshes))
    print('Loaded %s\t(%d meshes, %d faces)' % (file, len(meshes), size))
    return meshes

#We load the texture in addition to the skin effect
def load_skinned(file, shader, tex_file=None):
    try:
        pp = assimpcy.aiPostProcessSteps
        flags = pp.aiProcess_Triangulate | pp.aiProcess_GenSmoothNormals
        scene = assimpcy.aiImportFile(file, flags)
    except assimpcy.all.AssimpError as exception:
        print('ERROR loading', file + ': ', exception.args[0].decode())
        return []

    def conv(assimp_keys, ticks_per_second):
        """ Conversion from assimp key struct to our dict representation """
        return {key.mTime / ticks_per_second: key.mValue for key in assimp_keys}
    transform_keyframes = {}
    if scene.mAnimations:
        anim = scene.mAnimations[0]
        for channel in anim.mChannels:
            transform_keyframes[channel.mNodeName] = (
                conv(channel.mPositionKeys, anim.mTicksPerSecond),
                conv(channel.mRotationKeys, anim.mTicksPerSecond),
                conv(channel.mScalingKeys, anim.mTicksPerSecond)
            )
    nodes = {}                                      
    nodes_per_mesh_id = [[] for _ in scene.mMeshes]  
    def make_nodes(assimp_node):
        trs_keyframes = transform_keyframes.get(assimp_node.mName, (None,))
        skin_node = SkinningControlNode(*trs_keyframes,
                                        transform=assimp_node.mTransformation)
        nodes[assimp_node.mName] = skin_node
        for mesh_index in assimp_node.mMeshes:
            nodes_per_mesh_id[mesh_index].append(skin_node)
        skin_node.add(*(make_nodes(child) for child in assimp_node.mChildren))
        return skin_node
    root_node = make_nodes(scene.mRootNode)
    path = os.path.dirname(file) if os.path.dirname(file) != '' else './'
    for mat in scene.mMaterials:
        if not tex_file and 'TEXTURE_BASE' in mat.properties:  
            name = os.path.basename(mat.properties['TEXTURE_BASE'])
            paths = os.walk(path, followlinks=True)
            found = [os.path.join(d, f) for d, _, n in paths for f in n
                    if name.startswith(f) or f.startswith(name)]
            assert found, 'Cannot find texture %s in %s subtree' % (name, path)
            tex_file = found[0]
        if tex_file:
            mat.properties['diffuse_map'] = Texture(tex_file=tex_file)
    for mesh_id, mesh in enumerate(scene.mMeshes):
        mat = scene.mMaterials[mesh.mMaterialIndex].properties
        assert mat['diffuse_map'], "Trying to map using a textureless material"
        v_bone = np.array([[(0, 0)]*MAX_BONES] * mesh.mNumVertices,
                          dtype=[('weight', 'f4'), ('id', 'u4')])
        for bone_id, bone in enumerate(mesh.mBones[:MAX_BONES]):
            for entry in bone.mWeights:  
                v_bone[entry.mVertexId][bone_id] = (entry.mWeight, bone_id)
        v_bone.sort(order='weight')             
        v_bone = v_bone[:, -MAX_VERTEX_BONES:]  
        bone_nodes = [nodes[bone.mName] for bone in mesh.mBones]
        bone_offsets = [bone.mOffsetMatrix for bone in mesh.mBones]
        attrib = [mesh.mVertices, mesh.mNormals, v_bone['id'], v_bone['weight'], mesh.mTextureCoords[0]]
        mesh = SkinnedMesh(shader, attrib, bone_nodes, bone_offsets, mat['diffuse_map'],mesh.mFaces)
        for node in nodes_per_mesh_id[mesh_id]:
            node.add(mesh)
    nb_triangles = sum((mesh.mNumFaces for mesh in scene.mMeshes))
    print('Loaded', file, '\t(%d meshes, %d faces, %d nodes, %d animations)' %
          (scene.mNumMeshes, nb_triangles, len(nodes), scene.mNumAnimations))
    return [root_node]

#---------------------------------------------------------------------Classes to load all objects of our scene----------------------------------------------------------
class Ground(Node):
    def __init__(self, shader):
        super().__init__()
        self.add(*load_textured('./pictures/finalground.obj', shader, './pictures/grass1.jpg'))

class Archer(Node):
    def __init__(self, shader):
        super().__init__()
        self.add(*load_textured('./pictures/archer_standing.FBX', shader, './pictures/archer.tga'))

class Castle(Node):
    def __init__(self, shader):
        super().__init__()
        self.add(*load_textured('./pictures/casstle_gate.FBX', shader, './pictures/csstle_gate_D.jpg'))

class Arrow(Node):
    def __init__(self, shader):
        super().__init__()
        self.add(*load_textured('./pictures/arrow.FBX', shader))

class Armored(Node):
    def __init__(self, shader):
        super().__init__()
        self.add(*load_skinned('./pictures/armored_swordman_standing.FBX', shader, './pictures/armored_swordman_team_color.tga'))

class SimpleMan(Node):
    def __init__(self, shader):
        super().__init__()
        self.add(*load_textured('./pictures/worker_standing.FBX', shader, './pictures/texture_worker.tga'))

class Target(Node):
    def __init__(self, shader, light_dir):
        super().__init__()
        self.add(*load_phong_mesh('./pictures/Target.fbx', shader, light_dir))

class Square(Node): #Elements which form the catapult
    def __init__(self, shader):
        super().__init__()
        self.add(*load_textured('./pictures/square.obj', shader, './pictures/wood.jpg'))

class Ball(Node):
    def __init__(self, shader):
        super().__init__()
        self.add(*load_textured('./pictures/boule2.obj', shader, './pictures/boule.jpg'))

class Wolves(Node):
    def __init__(self, shader):
        super().__init__()
        self.add(*load_textured('./pictures/wolves.obj', shader, './pictures/wolves.jpg'))

class Car(Node):
    def __init__(self, shader):
        super().__init__()
        self.add(*load_textured('./pictures/CAR/car_bottom.obj', shader, './pictures/CAR/yellow.png'))
        self.add(*load_textured('./pictures/CAR/car_roof.obj', shader, './pictures/CAR/yellow.png'))
        self.add(*load_textured('./pictures/CAR/car_wheel1.obj', shader, './pictures/CAR/rubber.jpg'))
        self.add(*load_textured('./pictures/CAR/car_wheel2.obj', shader, './pictures/CAR/rubber.jpg'))
        self.add(*load_textured('./pictures/CAR/car_wheel3.obj', shader, './pictures/CAR/rubber.jpg'))
        self.add(*load_textured('./pictures/CAR/car_wheel4.obj', shader, './pictures/CAR/rubber.jpg'))

class Stick(Node):
    def __init__(self, shader):
        super().__init__()
        self.add(*load_textured('./pictures/petit_baton.obj', shader, './pictures/branche.jpg'))

class Sky(Node):
    def __init__(self, shader):
        super().__init__()
        self.add(*load_textured('./pictures/flip_cube.obj', shader, './pictures/skygazon3.png'))

#--------------------------------------------------------------------------------# MAIN PROGRAM #-----------------------------------------------------------------------
def main():
   
    viewer = Viewer()

#--------------------------------------------------------------------------------------SHADERS--------------------------------------------------------------------------

    #Color for the ground using Phong's Model
    color_shader = Shader("phong.vert", "phong.frag")
    #Shader for textured object
    shader = Shader("texture.vert", "texture.frag")
    #Shader for skinning
    skin_shader = Shader("skinning.vert", "color.frag")

#-------------------------------------------------------------------------------------THE SCENE-------------------------------------------------------------------------
#The Ground
    light_dir = (1,1,0)
    ground = Ground(shader)
    viewer.add(ground)

#The Skybox
    sky = Sky(shader)
    base_sky = Node(transform=translate(0,240,0)@scale(350,350,350))
    base_sky.add(sky)
    viewer.add(base_sky)

#Archers
    #1
    archer = Archer(shader)
    translate_keys = {0: vec(100, 0, -80)}
    rotate_keys = {0: quaternion_from_euler(0,-125,-90)}
    scale_keys = {0: 0.1} 
    keynode1 = KeyFrameControlNode(translate_keys, rotate_keys, scale_keys)
    keynode1.add(archer)
    viewer.add(keynode1)
    #2
    translate_keys = {0: vec(140, 0, -80)}
    rotate_keys = {0: quaternion_from_euler(0,-125,-90)}
    scale_keys = {0: 0.1}
    keynode7 = KeyFrameControlNode(translate_keys, rotate_keys, scale_keys)
    keynode7.add(archer)
    viewer.add(keynode7)
    
#Animation of the two arrows
    arrow = Arrow(shader)
    translate_keys = {0: vec(130, 40, -90), 3: vec(180, 113, -120), 6: vec(220, 140, -160), 8: vec(260, 128, -200), 10: vec(290, 80, -250)}
    rotate_keys = {0: quaternion_from_euler(45,130,0), 3: quaternion_from_euler(22,130,0), 6: quaternion_from_euler(0,130,0), 9: quaternion_from_euler(-22,130,0), 12: quaternion_from_euler(-45,130,0)}
    scale_keys = {0: 0.3, 3: 0.3, 6: 0.3, 9: 0.3, 12: 0.3}
    keynode = KeyFrameControlNode(translate_keys, rotate_keys, scale_keys)
    keynode.add(arrow)
    viewer.add(keynode)

    translate_keys = {8: vec(180, 40, -90), 10: vec(200, 100, -120), 12: vec(220, 130, -160), 14: vec( 240, 110,-200), 16: vec(290,90,-250)}
    rotate_keys = {8: quaternion_from_euler(45,130,0), 10: quaternion_from_euler(22,130,0), 12: quaternion_from_euler(0,130,0), 14: quaternion_from_euler(-22,130,0), 16: quaternion_from_euler(-45,130,0)}
    scale_keys = {8: 0.3, 10: 0.3, 12: 0.3, 14:0.3, 16: 0.3}
    keynode7 = KeyFrameControlNode(translate_keys, rotate_keys, scale_keys)
    keynode7.add(arrow)
    viewer.add(keynode7)
    
#Target
    target = Target(color_shader, light_dir)
    translate_keys = {0: vec(300, 48, -250)}
    rotate_keys = {0: quaternion_from_euler(0,-45,-90)}
    scale_keys = {0: 80}
    keynode4 = KeyFrameControlNode(translate_keys, rotate_keys, scale_keys)
    keynode4.add(target)
    viewer.add(keynode4)

#Armored Swordmen
    #1
    armored = Armored(skin_shader)
    translate_keys = {0: vec(-110, 0, -225)}
    rotate_keys = {0: quaternion_from_euler(0,-45,0)}
    scale_keys = {0: 0.2}
    keynode3 = KeyFrameControlNode(translate_keys, rotate_keys, scale_keys)
    keynode3.add(armored)
    viewer.add(keynode3)
    #2
    translate_keys = {0: vec(-220, 0, -120)}
    rotate_keys = {0: quaternion_from_euler(0,-45,0)}
    scale_keys = {0: 0.2}
    keynode5 = KeyFrameControlNode(translate_keys, rotate_keys, scale_keys)
    keynode5.add(armored)
    viewer.add(keynode5)

#Castle
    castle = Castle(shader)
    translate_keys = {0: vec(-130, 0, -280)}
    rotate_keys = {0: quaternion_from_euler(0,45,-90)}
    scale_keys = {0: 2}
    keynode2 = KeyFrameControlNode(translate_keys, rotate_keys, scale_keys)
    keynode2.add(castle)
    viewer.add(keynode2)

#Hierarchical catapult
    #Shape of the catapult
    square = Square(shader)

    base_shape = Node(transform=translate(225,5,250)@rotate((0,1,0),90)@scale(15,15,15))
    base_shape.add(square)
    viewer.add(base_shape)

    arm_shape = Node(transform=translate(225,17,250)@rotate((0,0,1),90)@scale(13,7,6))
    arm_shape.add(square)
    viewer.add(arm_shape)

    forearm_shape = Node(transform=translate(225,34,250)@rotate((0,1,0),90)@scale(4,4,20))
    forearm_shape.add(square)
    rotation_forearm = RotationControlNode(glfw.KEY_UP, glfw.KEY_DOWN, (225, 34, 250))
    rotation_forearm.add(forearm_shape)
    viewer.add(rotation_forearm)
    
    #Balls for the catapult
    circle = Ball(shader)
    base = Node(transform=translate(280,40,250)@scale(5,5,5))
    base.add(circle)
    t = TranslationControlNode(glfw.KEY_LEFT, glfw.KEY_RIGHT, 0, 0, 0)
    t.add(base)
    viewer.add(t)

    base1 = Node(transform=translate(280,5,220)@scale(5,5,5))
    base1.add(circle)
    viewer.add(base1)

    base2 = Node(transform=translate(290,5,210)@scale(5,5,5))
    base2.add(circle)
    viewer.add(base2)

    base3 = Node(transform=translate(285,5,200)@scale(5,5,5))
    base3.add(circle)
    viewer.add(base3)

    #Man using the catapult
    translate_keys = {0: vec(300, 5, 275)}
    rotate_keys = {0: quaternion_from_euler(0,0,-90)}
    scale_keys = {0: 0.1}
    keynode6 = KeyFrameControlNode(translate_keys, rotate_keys, scale_keys)
    keynode6.add(SimpleMan(shader))
    viewer.add(keynode6)

#Wolves
    wolves = Wolves(shader)
    base_wolves = Node(transform=translate(-290,40,230)@rotate((1,0,0),-90)@scale(0.5,0.5,0.5))
    base_wolves.add(wolves)
    viewer.add(base_wolves)

    base_wolves1 = Node(transform=translate(-290,5,330)@rotate((1,0,0),-90)@rotate((0,0,1),90)@scale(0.5,0.5,0.5))
    base_wolves1.add(wolves)
    viewer.add(base_wolves1)

#The car
    base_car = Node(transform=translate(0,3,0)@rotate((0,1,0),-115)@scale(25,25,25))
    car = Car(shader)
    base_car.add(car)
    viewer.add(base_car)

#Man attacking a car
    man = SimpleMan(shader)
    base_man = Node(transform=translate(-60,0,30)@rotate((0,0,1),-90)@rotate((0,1,0), -90)@scale(0.1,0.1,0.1))
    base_man.add(man)
    viewer.add(base_man)

#The wooden Stick
    baton = Stick(shader)
    base_baton = Node(transform=translate(-53,27,28)@scale(9,13,9))
    base_baton.add(baton)
    rotation = RotationControlNode(glfw.KEY_C, glfw.KEY_B, (-53, 27, 28))
    rotation.add(base_baton)
    viewer.add(rotation)


    if len(sys.argv) != 2:
        print('Usage:\n\t%s [3dfile]*\n\n3dfile\t\t the filename of a model in'
              ' format supported by assimp.' % (sys.argv[0],))

    print("\n")
    print("-------------- HOW TO USE OUR SCENE ----------------")
    print("PRESS UP and DOWN arrows on the keyboard to make it rotate from bottom to top.")
    print("PRESS LEFT and RIGHT arrows on the keyboard to make the ball go forward and higher.")
    print("PRESS R on the keyboard to relaunch the arrows.")
    print("PRESS C and B on the keyboard to move the stick.")

    # start rendering loop
    viewer.run()


if __name__ == '__main__':
    glfw.init()                
    main()                     
    glfw.terminate()           
